Package containing all launch files for the raven system.

No code belongs here -- only configuration.
(And the physical description of Raven belongs in raven_description and raven_gazebo.)

To install packages needed for running a Gazebo simulation, use raven_gazebo.repos:

~~~
cd ~
mkdir -p gazebo_ws/src
cd gazebo_ws/src
git clone git@gitlab.com:apl-ocean-engineering/raven/raven_config.git
vcs import --input raven_config/raven_gazebo.repos .
cd ..
rosdep install --from-paths src --ignore-src -r -y
source /opt/ros/noetic/setup.bash
catkin build
source devel/setup.bash
roslaunch raven_config raven.launch gazebo:=true
~~~
