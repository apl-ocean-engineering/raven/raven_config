For now, this directory is a holding place for sensor configuration.

### DVL

Greensea does NOT configure the DVL as part of their launch process, so we'll
want to have an easy and repeatable way to configure it.

There are a number of parameters that have been set for benchtesting that are not
appropriate for actual vehicle use:
* power level: -20dB for bucket testing; want higher for tank/lake
* ping rate: 1Hz for bucket; lake value will be tradeoff between update rate + max range
* water velocity + salinity

These values can all be changed either by the web interface or the telnet interface.

Current procedure to generate config files:
* Go to 10.10.10.132 (DVL's address on the network)
* "Instrument Configuration" tab
* Change any values in the gui
* click "Submit"
* click "Save Config" -> This will read out what is currently on the instrument. Double-check that it matches expected changes.

To upload a config file:
* Click "Upload Config" -- oddly, the browser seems busted and can only upload files that have been copied to the Desktop.
