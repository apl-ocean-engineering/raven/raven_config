All of the calibration metadata that will be required to launch the Raven.

For now, only includes in-air camera calibration, though eventually this will include:
* in-water camera calibration for each camera
* stereo calibration
* camera-oculus calibration
