<?xml version="1.0"?>
<launch>

  <!-- Assumes that the command line argument has one of these set to true:
     * gazebo - run all nodes required for gazebo simulation
     * gsssim - run all nodes required for gss simulation
     * robot - run all nodes for operating in AUV mode on the robot
  -->
  <!-- TODO: I wish that roslaunch had a way to specify that exactly
             one of these must be true. -->
  <arg name="gazebo" default="false"/>   <!-- Gazebo with ROS control stack-->
  <arg name="gazebo_teleport" default="false"/>   <!-- Use teleportation plugin -->
  <arg name="gazebo_exploration" default="false"/>   <!-- no drivers, no greensea, but perception -->
  <arg name="gsssim" default="false"/>  <!-- greensea interface, no perception -->
  <arg name="gssreplay" default="false"/>  <!-- Replay LCM logfiles + translator + clock generation. Does not combine with ROS replay for perception/planning. -->
  <arg name="robot" default="false"/>  <!-- greensea interface (including control) + sensor drivers -->
  <arg name="tank" default="false"/>  <!-- sensor drivers + perception only -->
  <arg name="replay" default="false"/>  <!-- only perception nodes, since we don't log their output -->

  <!-- Sometimes, we want the ability to disable individual components that
       would otherwise appear in a given configuration.
       TODO: For now, this only applies to the tank setup. Consider refactoring
             to only include each file once, to make it easier to pass these
	     flags into the individual subsystem launch files.
  -->
  <arg name="disable_lights" default="false"/>  <!-- Don't start light drivers -->
  <arg name="disable_oculus" default="false"/>  <!-- Don't start the Oculus sonar driver -->

  <!-- Topics that various nodes need to agree on. -->
  <arg name="joint_states_topic" default="/raven/joint_states"/>

  <!-- ALWAYS launch these components -->

  <!-- Load the URDF into the ROS Parameter Server -->
  <!-- (The below is the only way I could figure out how to implement
        "if, else if, else" logic in xml)-->
  <group if="$(arg gazebo_teleport)">
    <!-- start the simulation with the teleport plugins -->
    <param name="robot_description"
      command="$(find xacro)/xacro '$(find raven_description)/urdf/raven.teleport.xacro'" />
  </group>
  <group unless="$(arg gazebo_teleport)">
    <group if="$(arg gazebo)">
      <!-- start the simulation with the hydrodynamics plugins -->
      <param name="robot_description"
        command="$(find xacro)/xacro '$(find raven_description)/urdf/raven.hydrodynamics.xacro'" />
    </group>
    <group unless="$(arg gazebo)">
      <!-- If we're not in simulation, don't force those packages to be installed. -->
      <param name="robot_description"
        command="$(find xacro)/xacro '$(find raven_description)/urdf/raven.xacro'" />
    </group>
  </group>

  <!-- Publish a full tf tree for the robot, gathering static transforms
       from the /robot_description parameter, and active joints from
       /joint_states (published by ros_control's joint_state_controller) -->
  <node name="robot_state_publisher" pkg="robot_state_publisher"
        type="robot_state_publisher">
    <remap from="/joint_states" to="$(arg joint_states_topic)"/>
    <param name="publish_frequency" value="10"/>
  </node>

  <!-- Show in Rviz   -->
  <node name="rviz" pkg="rviz" type="rviz"
        args="-d $(find raven_config)/launch/raven.rviz"/>
  <!-- args="-d $(find sonar_occupancy_map)/rviz/octomap.rviz"/> -->

  <!-- MODE-DEPENDENT subsets of the system are defined in separate launch files. -->
  <!-- TODO: there are other subsets that I need to define, including: gss_translator, rqt viewer of multiple image views... -->

  <group if="$(arg gazebo)">
    <include file="$(find raven_config)/launch/gazebo.xml"/>
    <include file="$(find raven_config)/launch/gazebo_controller.xml"/>
    <include file="$(find raven_config)/launch/gazebo_tf.xml"/>

    <include file="$(find raven_config)/launch/joy_node.xml"/>
    <node pkg="raven_control" type="raven_teleop.py" name="raven_teleop">
      <remap from="/teleop_wrench" to="/raven/wrench_command"/>
    </node>
    <group ns="raven">
      <rosparam ns="thruster_geometry" command="load" file="$(find raven_control)/launch/thruster_geometry.yaml"/>
      <rosparam ns="thruster_cmd_rpms" command="load" file="$(find raven_control)/launch/thruster_cmd_rpms.yaml"/>
      <node pkg="raven_control" name="thrust_allocation" type="thrust_allocation.py"/>
    </group>

    <node pkg="raven_sim_control" type="thrust_demuxer.py" name="thrust_demuxer">
      <remap from="/cmd" to="/raven/thruster_command"/>
    </node>

    <!--
    <include file="$(find raven_config)/launch/perception.xml"/>
    -->
  </group>

  <group if="$(arg gazebo_teleport)">
    <include file="$(find raven_config)/launch/gazebo.xml"/>
    <include file="$(find raven_config)/launch/gazebo_controller.xml"/>
    <include file="$(find raven_config)/launch/gazebo_tf.xml"/>

    <include file="$(find raven_config)/launch/joy_node.xml"/>

    <group>
      <param name="~cmd_frame" value="world_gazebo"/>
      <include file="$(find raven_config)/launch/sim_teleop.xml"/>
      <include file="$(find raven_config)/launch/sim_carrot.xml"/>
      <include file="$(find raven_config)/launch/sim_arbiter.xml"/>
    </group>

    <!--
    <include file="$(find raven_config)/launch/perception.xml"/>
    -->
  </group>
  <group if="$(arg gazebo_exploration)">
    <include file="$(find raven_config)/launch/gazebo.xml"/>
    <include file="$(find raven_config)/launch/gazebo_tf.xml"/>
    <include file="$(find raven_config)/launch/gazebo_controller.xml"/>

    <group ns="sim_control">
      <param name="~cmd_frame" value="world_gazebo"/>
      <include file="$(find raven_config)/launch/sim_teleop.xml"/>
      <include file="$(find raven_config)/launch/sim_carrot.xml"/>
      <include file="$(find raven_config)/launch/sim_arbiter.xml"/>
    </group>

    <include file="$(find raven_config)/launch/perception.xml"/>
    <include file="$(find path_planner)/launch/planning_pipeline.launch" />
  </group>

  <group if="$(arg gsssim)">
    <node name="gss_translator" pkg="greensea_translator" type="gss_translator"/>
  </group>

  <group if="$(arg gssreplay)">
    <node name="gss_translator" pkg="greensea_translator" type="gss_translator"/>
    <!-- TODO: need to take GSS's timestamps and publish them on /clock -->
    <!-- TODO: set use_sim_time to true! -->
  </group>

  <group if="$(arg robot)" ns="raven">
    <node name="gss_translator" pkg="greensea_translator" type="gss_translator"/>

    <node name="vib2_driver" pkg="vib2_driver" type="vib2">
      <param name="vib_ip" type="string" value="vib.raven.apl.uw.edu"/>
      <param name="update_rate" type="double" value="1.0"/>
      <param name="frame" type="string" value="raven"/>
    </node>

    <node name="raven_executive" pkg="raven_control" type="raven_executive.py">
      <param name="xy_tol" value="0.05"/>
      <param name="depth_tol" value="0.1"/>
      <param name="heading_tol" value="5"/>
    </node>

    <!-- Currently using a waypoint controller that runs on the vehicle.
    <node pkg="raven_control" type="raven_teleop.py" name="raven_teleop">
      <remap from="teleop_wrench" to="wrench_command"/>
    </node>

    <group>
      <rosparam ns="thruster_geometry" command="load" file="$(find raven_control)/launch/thruster_geometry.yaml"/>
      <rosparam ns="thruster_cmd_rpms" command="load" file="$(find raven_control)/launch/thruster_cmd_rpms.yaml"/>
      <node pkg="raven_control" name="thrust_allocation" type="thrust_allocation.py"/>
    </group>
    -->

    <node name="rqt" pkg="rqt_gui" type="rqt_gui" args="--perspective-file $(find raven_config)/launch/raven.perspective"/>

    <include file="$(find raven_config)/launch/md_actuator.xml">
      <arg name="joint_states_topic" value="$(arg joint_states_topic)"/>
    </include>

    <include file="$(find raven_config)/launch/sensors.xml">
      <arg name="on_raven" value="true"/>
      <arg name="disable_lights" value="$(arg disable_lights)"/>
      <arg name="disable_oculus" value="$(arg disable_oculus)"/>
    </include>

      <include file="$(find raven_config)/launch/perception.xml">
        <arg name="base_ns" value="/raven"/>
      </include>
    <!--
      <include file="$(find path_planner)/launch/planning_pipeline.launch" />
    -->
      <!-- the '/' is important, becasue all downstream usages of the ns
           are going to want it to be the absolute path.
      -->
  </group>  <!-- if robot -->

  <group if="$(arg tank)" ns="raven">
    <!-- TODO: aruco-based position estimator -->

    <!-- In the tank, we often want to disable specific devices
         depending on what we're debugging. -->
    <include file="$(find raven_config)/launch/sensors.xml">
      <arg name="on_raven" value="false"/>
      <arg name="disable_lights" value="$(arg disable_lights)"/>
    </include>

    <!-- Similarly, we'll sometimes want to disable pieces of the perception pipeline. -->
    <!--
    <include file="$(find raven_config)/launch/perception.xml">
      <arg name="base_ns" value="/raven"/>
    </include>
    -->
  </group>  <!-- if tank -->

  <group if="$(arg replay)" ns="raven">
    <include file="$(find raven_config)/launch/perception.xml">
      <arg name="base_ns" value="/raven"/>
    </include>
  </group>


</launch>
