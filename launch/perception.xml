<?xml version="1.0"?>

<launch>

  <!-- roslaunch doesn't make it possible to access the namespace from
       within a launch file, so we pass it along. -->
  <arg name="base_ns" default=""/>

  <!-- sonar processing pipelines -->
  <!-- Marc & Laura's reconstruction code -->
  <arg name="sonar_image_topic" default="/raven/oculus/sonar_image"/>
  <arg name="deterministic_replay" default="false"/>
  <arg name="method" default="sonar_occupancy_node"/>
  <arg name="color_map" default="Magma"/>
  <arg name="resolution" default="0.10"/>
  <arg name="map_frame" default="gss_origin"/>

  <group ns="reconstruction">
      <node name="sonar_occupancy_node" type="$(arg method)" pkg="octomap_reconstruction" output="screen">
          <remap from="/sonar_image" to="$(arg sonar_image_topic)"/>
          <param name="deterministic_replay" value="$(arg deterministic_replay)"/>
          <param name="color_map" type="string" value="$(arg color_map)"/>
          <param name="map_frame" type="string" value="$(arg map_frame)"/>
          <param name="resolution" type="double" value="$(arg resolution)"/>
          <param name="center_x" type="double" value="0.0"/>
          <param name="center_y" type="double" value="0.0"/>
          <param name="center_z" type="double" value="0.0"/>
          <param name="occupancy_threshold" type="double" value="0.45"/>
      </node>
  </group>

  <!-- OSU's sonar map -->
  <!--
  <node name="sonar_occupancy_map" pkg="sonar_occupancy_map" type="sonar_occupancy_map_node" output="screen">
    <rosparam command="load" file="$(find sonar_occupancy_map)/config/default.yaml" />
  </node>
  -->

  <!-- stereo processing pipelines -->

  <!-- NB: The stereo/{left,right} namespaces must match sensors.xml -->
  <!-- QUESTION: Is that an antipattern, such that drivers + stereo processing
       should be in the same launch file? -->
  <group ns="stereo">
    <!-- downsample the images before trying to calculate disparity -->
    <!-- QUESTION: Would it be better to use stereo_downsampled?
         stereo/{left,right} is somewhat equivalent to stereo/downsampled/{left,right},
         suggesting that they ought to be at the same level in the namespace tree.
         However, then we'd have the issue that we no longer have "stereo"
         as the namespace that we use for everything related to the stereo
         pair on the sensor head.
         (I really don't want to add another layer of nesting ... that's
         excessive typing on the command-line debugging tools...)
    -->
    <arg name="downsampled_ns" value="$(arg base_ns)/stereo/downsampled"/>
    <arg name="stereo_ns" value="$(arg base_ns)/stereo"/>

    <group ns="left">
      <node name="crop_decimate" type="nodelet" pkg="nodelet" args="standalone image_proc/crop_decimate">
        <!-- ugggh. There is no facility for specifying relative namespaces.
             So either camera or camera_out has to have it fully-specified
             (or both do, and we don't use a namespace to differentiate
             between left/right)
             Additionally, stereo_image_proc assumes that left/right will
             adjacent in the same namespace.
        -->
        <remap from="camera" to="$(arg stereo_ns)/left"/>
        <remap from="camera_out" to="$(arg downsampled_ns)/left"/>
        <param name="decimation_x" type="int" value="4"/>
        <param name="decimation_y" type="int" value="4"/>
      </node>
    </group>  <!-- ns="left" -->

    <group ns="right">
      <node name="crop_decimate" type="nodelet" pkg="nodelet" args="standalone image_proc/crop_decimate">
        <remap from="camera" to="$(arg stereo_ns)/right"/>
        <remap from="camera_out" to="$(arg downsampled_ns)/right"/>
        <param name="decimation_x" type="int" value="4"/>
        <param name="decimation_y" type="int" value="4"/>
      </node>
    </group>  <!-- ns=right -->

    <node name="stereo_image_proc" type="stereo_image_proc" pkg="stereo_image_proc" ns="$(arg downsampled_ns)">
      <param name="approximate_sync" type="bool" value="true"/>
      <!-- using dynamic_reconfigure, SGBM looks better. `rosparam get stereo_algorithm` returned 1...-->
      <param name="stereo_algorithm" type="int" value="1"/>
    </node>

  </group>  <!-- ns=stereo -->

</launch>
